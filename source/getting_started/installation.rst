.. _getting_started_installation:

Instalação
==========
Os passos a seguir mostram a forma básica para instalar e começar a utilizar a Fintecc SDK em seu projeto.

Composer
--------
Esse SDK deve ser instalado diretamente pelo composer,
por se tratar de um repositório VCS privado você deve inserir a referência desse repositório no *composer.json* do seu projeto:

.. code::

    "repositories": [
        {
            "type": "vcs",
            "url": "git@bitbucket.org:ambientedevfintecc/sdk.fintecc.com.br.git"
        }
    ]

E então utilizar o comando ``composer required fintecc/sdk``

Ou inserir a dependência diretamente no arquivo *composer.json*

.. code::

    "require": {
        "fintecc/sdk": "1.*"
    },

.. note:: Para instalar o fintecc/sdk a partir de um branch (não uma tag) coloque o prefixo **dev-**
          seguido do nome do branch desejado, como **dev-bug_fix** por exemplo.

Em seguida bastar executar ``composer update fintecc/sdk``

E o SDK já estará disponível para utilização

para mais informações veja a documentação do `composer para repositórios VCS <https://getcomposer.org/doc/05-repositories.md#vcs>`_

Inicialização
-------------
A inicialização dos providers e aliases pode variar de acordo com a versão do Laravel,
veja como iniciar os providers para cada versão:

Laravel 5.5+
^^^^^^^^^^^^
O Laravel vai identificar a referências dos providers automaticamente.

Laravel 5.4-
^^^^^^^^^^^^
Basta fazer a referência de um único provider no arquivo *config/app.php*

.. code-block:: php

    <?php
    'providers' => [
        Fintecc\Providers\MainServiceProvider::class
    ]

Publicando
----------
Para publicar as configurações do SDK para sua aplicação basta executar ``php artisan vendor:publish`` no diretório do projeto,
isso fará com que todos os arquivos de configuração, blades, traduções, etc, sejam publicados em seu projeto.
Se você precisar atualizar esses arquivos após a instalação (devido a uma atualização do SDK por exemplo) você pode executar
``php artisan vendor:publish --force`` para forçar o Laravel a sobrescrever os arquivos.
