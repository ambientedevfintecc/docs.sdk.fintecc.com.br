.. _getting_started_authentication:

Autenticação
============
Essa sessão mostra como efetuar a autenticação utilizando qualquer API do grupo Fintecc.
Isso fará com que o Laravel não execute mais a autenticação pelo *Eloquent* mas acesse as APIs via *curl*.

Iniciando
---------
Primeiramente você precisa dizer ao Laravel para utilizar a autenticação da Fintecc.
Para isso, vá em ``config\auth.php`` e em *Authentication Guards* defina *fintecc*
como o provider a ser utilizado:

.. code-block:: php

    <?php
    'guards' => [
        'web' => [
            'driver' => 'session',
            'provider' => 'fintecc',
        ],

        //...
    ]

E em *User Providers* (no mesmo arquivo) defina o *driver* para *fintecc_auth* e diga o usuário que o driver deve utilizar.
Os usuários padrões para cada Empresa já estão disponíveis no SDK.

.. code-block:: php

    <?php
    'providers' => [
        //...

        'fintecc' => [
            'driver' => 'fintecc_auth',
            'model'  => <EMPRESA>\Models\User::class,
        ],

        //...
    ],

Com apenas isso a autenticação do Laravel já vai funcionar normalmente utilizando a API da Fintecc.

Autenticação Customizada
------------------------
Caso você queira uma autenticação customizada ou está criando uma nova instância da autenticação para o SDK,
basta criar uma nova model de usuário que estenda a model padrão da fintecc definindo a forma de conexão.

Por exemplo, digamos que você está criando um novo usuário para uma nova empresa,
basta criar uma classe que estenda ``Fintecc\Models\User``


.. code-block:: php

    <?php
    namespace NewCompany\Models;

    use Fintecc\Models\User as FinteccUser;

    class User extends FinteccUser
    {
        /** @inheritDoc */
        public static function defaultService()
        {
            return \NewCompany\Models\Service::class;
        }
    }

Sua nova classe deve **obrigatoriamente** definir um método estático com o nome ``defaultService``
que ira dizer ao SDK qual service de conexão com a API ele deve utilizar. Para mais informações veja: :ref:`usage_services`

.. note:: Você não precisa criar um service novo, você pode utilizar um já existente caso queira,
          basta referencia-lo no método ``defaultService`` da sua classe.

Caso sua nova classe não siga os padrões de Url e propriedades já definidos, você pode criar suas próprias propriedades e métodos de login.
Para tal, basta sobrescrever os métodos estáticos ``findById`` e ``login``:

.. code-block:: php

    <?php
    /**
     * @var string $minhaProp minha propriedade customizada
     * @translate minhaProp
     */
    private $myProp;

    /** @inheritDoc */
    public static function findById(string $id)
    {
        $service = static::defaultService();
        $response = (new $service)->get('minha/url/get/cliente/'.$id);

        $user = Domain::create(static::class, $response, true);
        $user->id = $id;

        return $user;
    }

    /** @inheritDoc */
    public static function login(array $credentials)
    {
        $service = static::defaultService();
        $response = (new $service)->post('/minha/url/de/autenticacao', $credentials);

        return Domain::create(static::class, $response, true);
    }

Para mais informações sobre como criar seus atributos veja :ref:`usage_domains`.
