.. _usage_domains:

Domains
=======
Domains são classes semânticas que contem um conjunto de informações que possuem, de alguma forma, valor para o sistema.

Um bom exemplo de domain é a classe de ``Cliente`` que possui um conjunto de informações como nome, email e cpf, por exemplo.

A estrutura de um domain consistem simplesmente em propriedades privadas ou protegidas que possuam seus respectivos meodos *Get* e *Set*, por exemplo:

.. code-block:: php

    <?php
    class MeuDomain
    {
        /**
        * @var string $id token de sessão do cliente
        * @translate clienteTokenId
        */
        private $id;

        /**
        * @var int $pureId id sequencial do cliente
        * @translate id
        */
        private $pureId;

        /**
        * @var string $status status atual do cliente
        * @translate statusId
        */
        private $status;

        /**
        * @var string $name nome completo do cliente
        * @translate pessoa->nome
        */
        private $name;

        //... Getters e Setters
    }

Criando um domain
-----------------
Para criar um domain você pode usar o método ``create`` da classe ``Fintecc\Domains\Domain``,
que recebe como parâmetro a classe que o deve ser instanciada, as informações a serem inseridas no domain
e um boolean que define se as propriedades devem ser traduzidas com o padrão definido em ``@translate`` de cada propriedade

.. code-block:: php

    <?php
    Domain::create(MeuDomain::class, $data, true);

@translate
----------
Ao colocar a tag ``@translate`` no comentário de sua propriedade você está dizendo ao método ``create``
como definir essa propriedade quando vinda de alguma lugar que deve ser traduzida.

Por exemplo, digamos que ao fazer um **GET** de um usuário na API a propriedade *name* venha como *nomeDoUsuario*,
colocando ``@translate nomeDoUsuario`` no comentário da propriedade já faz com ela seja traduzida ao ser criada.

.. warning::
    A função ``create`` assume que suas propriedades estão acessíveis por meio dos métodos *Get* e *Set*,
    caso sua propriedade não possua esses métodos ela será **ignorada**.

Caso você queira traduzir a propriedade a partir de um array associativo ou de um objeto basta utilizar o operador de objeto ``->``.
Por exemplo, ``@translate usuario->nome``. E isso pode ocorrer de forma recursiva: ``@translate usuario->dados->detalhes->nome``

Caso sua propriedade consiste na concatenação de duas ou mais propriedades do seu array associativo/objeto
você pode dizer isso ao ``create`` ao inserir o operador de adição ``+`` no ``@translate`` da sua propriedade.
Por exemplo:

.. code-block:: php

    <?php
    class MeuDomain
    {
        /**
        * @var string $phone numero de telefone do cliente
        * @translate telefone->ddd + telefone->numero
        */
        private $phone;

        //... Getters e Setters
    }

O exemplo acima vai inserir o que estiver dentro de:

.. code::

    {
        "telefone" : {
            "ddd"    : 11,
            "numero" : 123456789
        }
    }

Em uma única propriedade ``phone`` do seu Domain, então o valor dessa propriedade após criada será ``11123456789``

Isso não se limita as propriedades dentro de uma mesma *key* você pode fazer algo como:

.. code-block:: php

    <?php
    class MeuDomain
    {
        /**
        * @var string $phone numero de telefone do cliente
        * @translate paises->codigos->brasil + cidades->saoPaulo->ddd + telefone->numero
        */
        private $phone;

        //... Getters e Setters
    }

Se seu array/objeto for:

.. code::

    {
        "paises"   : {
            "codigos" : {
                "brasil" : "+55"
            }
        },
        "cidades"    : {
            "saoPaulo" : {
                "ddd" : 11
            }
        },
        "telefone" : {
            "numero" : 123456789
        }
    }


Então o valor de sua propriedade após criada será ``+5511123456789``

@translate para Array
---------------------
O caminho inverso para o ``@translate`` é feito pelo método ``toArray`` da classe ``Fintecc\Domains\Domain``.

Esse método faz com que a classe seja convertida para um array associativo traduzindo suas propriedades
conforme descrito na documentação ``@translate``.

Por exemplo, se sua classe for algo como:

.. code-block:: php

    <?php
    class MeuDomain
    {
        /**
        * @var string $id token de sessão do cliente
        * @translate clienteTokenId
        */
        private $id;

        /**
        * @var int $pureId id sequencial do cliente
        * @translate id
        */
        private $pureId;

        /**
        * @var string $status status atual do cliente
        * @translate statusId
        */
        private $status;

        /**
        * @var string $name nome completo do cliente
        * @translate pessoa->nome
        */
        private $name;

        /**
        * @var string $phone numero de telefone do cliente
        * @translate telefone->ddd + telefone->numero
        */
        private $phone;

        //... Getters e Setters
    }

E você invocar o método ``toArray`` passando o segundo parâmetro como true (para que a tradução seja feita):

.. code-block:: php

    <?php
    $meuObjeto = new MeuDomain();
    $array = Domain::toArray($meuObjeto, true);

A variável ``$array`` será algo como:

.. code::

    {
        "clienteTokenId" : "",
        "id"             : "",
        "statusId"       : "",
        "pessoa"         : {
            "nome" : ""
        },
        "telefone"       : {
            "ddd"    : "",
            "numero" : ""
        }
    }

.. note:: como já abordado na sessão anterior sobre o método ``create``
          o método ``toArray`` também assume que suas propriedades são acessíveis a partir de métodos *Get* e *Set*
          e caso a propriedade não possua esses métodos ela será **ignorada**.

Propriedades concatenadas para o toArray
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
Caso a propriedade do seu objeto consista de uma concatenação de propriedades (como o telefone no exemplo anterior)
o método *Get* da propriedade deve saber como retornar os valores separadamente recebendo como parâmetro o *index*
de cada propriedade concatenada respectivamente.

Por exemplo, digamos que seu *translate* seja ``@translate telefone->ddd + telefone->numero``
o método get dessa propriedade será chamado duas vezes, uma vez passando como parâmetro o inteiro ``0``
e uma segunda vez passando como parâmetro o inteiro ``1`` que representam ``telefone->ddd`` e ``telefone->numero``
respectivamente.

.. code-block:: php

    <?php
    class MeuDomain
    {
     /**
        * @var string $phone numero de telefone do cliente
        * @translate telefone->ddd + telefone->numero
        */
        private $phone;

        /**
         * Get $phone telefone do cliente
         *
         * @param mixed $splitIndex caso exista será utilizado para retornar o valor parcial da propriedade
         * @return string
         */
        public function getPhone($splitIndex = null)
        {
            if (is_int($splitIndex)) {
                switch ($splitIndex) {
                    case 0:
                        return substr($this->phone, 0, 2);
                        break;

                    case 1:
                        return substr($this->phone, 2);
                        break;
                }
            }

            return $this->phone;
        }

        /**
         * Set $phone telefone do cliente
         *
         * @param string
         */
        public function setPhone(string $phone)
        {
            $this->phone = $phone;
        }
    }

.. note:: O código acima é apenas para ser tomado como exemplo, uma boa prática seria criar um novo método que devesse ser chamado
          caso o parâmetro ``$splitIndex`` não seja vazio/nulo.
