.. _about_index:

Sobre
=====
Entender um pouco sobre o objetivo desse projeto é essencial para começar a utilizá-lo.
Fintecc SDK é um pacote Laravel que visa adicionar as funcionalidades básicas e comuns dos projetos do grupo ao Framework.

.. note:: Consulte a documentação de `Pacotes Laravel <https://laravel.com/docs/master/packages>`_ para um maior entendimento

Introdução
----------
Esse SDK visa facilitar a implementação de projetos Fintecc disponibilizando recursos básicos
e comuns as aplicações em um único lugar. Com isso é possível usar classes básicas de autenticação ou cadastro, por exemplo,
sem a necessidade de escrever toda uma implementação nova para cada empresa/projeto.

Por que utilizar
----------------
Esse projeto pode agilizar, e muito, a implementação de novas features e a manutenção das já existentes.
Utilize esse SDK para todos os projetos que precisam utilizar qualquer API da Fintecc, a mesma estrutura de autenticação,
conexão com banco de dados ou simplesmente rápida referência para esses propósitos.
