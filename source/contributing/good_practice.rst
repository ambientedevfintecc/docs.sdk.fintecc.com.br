.. _contributing_good_practice:

Boas Práticas
=============
Leia atentamente cada paragrafo dessa sessão pois é de insuma importância para que você possa contribuir para o projeto.
Além de ajudar no entendimento do código.

Providers e Aliases
-------------------
Caso você crie um novo provider e/ou alias, eles devem ser registrado no ``Fintecc\Providers\MainServiceProvider``.
assim todas as versões do Laravel poderão utilizar o seu provider automaticamente após a instalação do SDK. Vide :ref:`getting_started_installation`

.. code-block:: php

    <?php
    /** @var array PROVIDERS define os providers a serem registrados na aplicação */
    const PROVIDERS = [
        \Namespace\Para\Seu\Provider::class,
    ];

    /** @var array ALIASES define os aliases a serem registrados na aplicação */
    const ALIASES = [
        'Alias' => \Namespace\Para\Seu\Alias::class,
    ];

Namespace
---------
Caso você crie uma nova empresa/namespace é necessário dizer ao composer como carrega-lo de acordo com a PSR-4, por exemplo:

.. code::

    "autoload": {
        "psr-4":{
            "Fintecc\\"    : ["/src/Fintecc"],
            "NewCompany\\" : ["/src/NewCompany"]
        }
    },

Comentários
-----------
Todo código no SDK está comentando utilizando os padrões do PHP Documentor.
Você pode consultar a `PSR do PHP Documentor`_ para saber exatamente como comentar seu código.

Nomenclaturas
-------------
Os padrões definidos pelo projeto são:

* Nome de variáveis e propriedades de classes devem seguir o padrão *camelCase*
* Constantes devem ser definidas com todas as letras maiúsculas e palavras devem ser separadas por underscore (*CONSTANTE_NOME*)

O restante do código **DEVE** seguir os padrões definidos na PSR-2. Caso você tenha alguma dúvida sobre PSRs ou como estruturar seu código,
consulte o guia `PHP Do Jeito Certo`_.

TAGs
----
As tags do projeto devem ser numeradas de uma forma que seja consistente com o `Versionamento Semântico`_

.. _`PSR do PHP Documentor`: https://github.com/phpDocumentor/fig-standards/blob/master/proposed/phpdoc.md
.. _`PHP Do Jeito Certo`: http://br.phptherightway.com/
.. _`Versionamento Semântico`: https://semver.org/lang/pt-BR/
