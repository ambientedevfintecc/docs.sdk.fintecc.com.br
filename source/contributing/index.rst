.. _contributing_index:

Contribuindo
============
Se você está seguindo essa documentações, provavelmente você pertence a uma das empresas do grupo Fintecc.
Sendo assim, qualquer contribuição para este projeto será bem vinda.

Como contribuir
---------------
Você pode criar novas funcionalidades, novos componentes, novas classes, refatorar o código ou criar novas implementações,
sinta-se livre para ajudar o projeto a crescer.

Caso você tenha acesso ao repositório diretamente, basta criar um novo branch e começar a escrever seus códigos,
quando tudo estiver pronto, basta fazer um pull request para a branch master através do bitbucket.
Se você não tem acesso direto ao repositório, você criar um fork do projeto e então fazer o pull request.

Escolha a forma que lhe for mais conveniente.

É importante que antes de começar a contribuir você leia :ref:`contributing_good_practice` para ter certeza que sua contribuição será aceita.

Fazendo o Pull request
----------------------
Após efetuar sua alteração (seja ela por branch ou fork) basta ir até o site do bitbucket e criar um novo pull request.

Caso não saiba como fazê-lo, ou deseja saber mais sobre este assunto veja a documentação do bitbucket para
`criar um pull request <https://www.atlassian.com/git/tutorials/making-a-pull request>`_
