.. Fintecc SDK documentation master file, created by
   sphinx-quickstart on Wed Feb 14 16:51:56 2018.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Fintecc SDK - *DEV* branch
==========================

.. warning:: Essa documentação é referente a uma versão instável (dev branch). Procurando pela documentação
             da versão estável atual? `Aqui está <http://fintecc-sdk.readthedocs.io/pt_BR/latest/>`_.

Bem vindo a documentação do Fintecc SDK, um pacote SDK Laravel fechado para as empresas do grupo Fintecc!
Este pacote poderoso, porem simples de ser utilizado, irá guiá-lo no desenvolvimento PHP Laravel, seja um
projeto simples ou uma aplicação completa e robusta, este SDK irá te prover o necessário para começar seu desenvolvimento
e auxilia-lo até o fim.

A tabela de conteúdo abaixo e no menu lateral irá ajuda-lo a ter um fácil acesso a todo conteúdo desta documentação.

.. note:: Esse projeto é privado, isso significa que você precisa ser membro do grupo Fintecc e ter acesso ao
          `Repositório <https://bitbucket.org/ambientedevfintecc/sdk.fintecc.com.br>`_ para utilizá-lo em seu projeto Laravel.

          Caso você encontre algum bug ou desenvolva uma melhoria, envie um issue ou Pull Request diretamente para o projeto no
          `Bitbucket <https://bitbucket.org/ambientedevfintecc/sdk.fintecc.com.br/issues>`_.

Essa documentação está organizada da seguinte forma:

.. toctree::
   :maxdepth: 1
   :caption: Geral:
   :name: sec-general

   about/index

.. toctree::
   :maxdepth: 1
   :caption: Contribuindo:
   :name: sec-contributing

   contributing/index
   contributing/good_practice

.. toctree::
   :maxdepth: 1
   :caption: Primeiros Passos:
   :name: sec-getting_started

   getting_started/installation
   getting_started/authentication

.. toctree::
   :maxdepth: 1
   :caption: Utilização:
   :name: sec-usage

   usage/services
   usage/domains
